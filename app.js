// Express
const express = require('express')
// Express Layouts
const expressLayouts = require('express-ejs-layouts')
// Express Session
const session = require('express-session')
// Flash
const flash = require('connect-flash')


// Initilizatio App
const app = express()

// BodyParser
app.use(express.urlencoded({ extended: false }) )

// EJS Middleware
app.use(expressLayouts)
app.set('view engine', 'ejs')

// Express Session
app.use(session({
    secret: 'secret',
    resave: false,
    saveUninitialized: true,
}))

// Middleware Flash
app.use(flash())
// Globar Varivale
app.use((req, res, next) => {
    res.locals.success_msg = req.flash('success_msg')
    res.locals.error_msg = req.flash('error_msg')
    res.locals.error = req.flash('error')
    res.locals.success_post = req.flash('success_post')
    next()
})


// Setting Routes
app.use('/', require('./routes/index'))
app.use('/users', require('./routes/users'))

// Getting PORT
const PORT = process.env.PORT || 5000

// Starting the server
app.listen(PORT, console.log('Server Started on PORT', PORT))